<?php
/**
 * @class VMUtc
 * Description of VMUtc class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMUtc extends CApplicationComponent
{
    const MYSQL_DATETIME = 'Y-m-d H:i:s';
    public $getTimezoneAction = '/default/getTimezone';

    /**
     * Get user local datetime offset from UTC in seconds
     *
     * @return integer $offset
     * @throws CException
     */
    public function currentDateTimeOffset()
    {
        $currentDatetimeOffset = Yii::app()->session->itemAt('currentDateTimeOffset');
        if (!$currentDatetimeOffset && !$this->getTimezoneAction) {
           //  throw new CException('Undefined current datetime offset');
	        return 0; // return UTC
        } elseif(!$currentDatetimeOffset && $this->getTimezoneAction){
            $url = Yii::app()->createAbsoluteUrl($this->getTimezoneAction,array(
                'url' => urlencode(Yii::app()->request->url)
            ));
            Yii::app()->controller->redirect($url);
        }

        return Yii::app()->session->itemAt('currentDateTimeOffset');
    }

    /**
     * Return string with datetime converted from user locale to UTC
     *
     * @param DateTime $datetime
     *
     * @return DateTime $datetime
     */
    public function toUTC($datetime)
    {
        $dateInterval = DateInterval::createFromDateString(sprintf("%d minutes", $this->currentDateTimeOffset()));
        $datetime->sub($dateInterval);

        return $datetime;
    }

    /**
     * Get string width datetime in user locale
     *
     * @param DateTime $datetime
     *
     * @return DateTime $datetime
     */
    public function fromUTC($datetime)
    {
        $dateInterval = DateInterval::createFromDateString(sprintf("%d minutes", $this->currentDateTimeOffset()));
        $datetime->add($dateInterval);

        return $datetime;
    }

} 