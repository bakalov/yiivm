<?php
/**
 * @class VMJsonServiceAccessControlFilter
 * Description of VMJsonServiceAccessControlFilter class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */

class VMJsonServiceAccessControlFilter extends CAccessControlFilter
{
	/**
	 * Denies the access of the user.
	 * This method is invoked when access check fails.
	 * @param IWebUser $user the current user
	 * @param string $message the error message to be displayed
	 */
	protected function accessDenied($user,$message)
	{
		Yii::app()->controller->respondWithError(Yii::t('vm', 'Access denied'), 403);
	}

}