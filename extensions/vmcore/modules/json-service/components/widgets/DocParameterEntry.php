<?php

class DocParameterEntry extends CWidget
{
    const OFFSET_SIZE = 4;
    public $parameters = array();
    public $level = 1;
    private $crossImageUrl;

    public function run()
    {

        $assets = Yii::app()->assetManager->publish(sprintf('%s/assets/', __DIR__));
        Yii::app()->clientScript->registerCssFile(sprintf('%s/css/doc.css', $assets));
        Yii::app()->clientScript->registerScriptFile(sprintf('%s/js/common.js', $assets));

        $result = null;

        end($this->parameters);
        $lastIndex = key($this->parameters);
        foreach ($this->parameters as $name => $value) {
            $result .= $this->contentForParameter($name, $value, $lastIndex == $name);
        }
        echo "{" . $result . $this->getBracketOffset() . "}";
    }

    private function contentForParameter($name, $value, $last)
    {
        $comma = $last ? '' : '<span class="comma">,</span>';
        $isOptionalValue = is_array($value) && in_array('optional', $value, true);
        $isArray = is_array($value) && in_array('array', $value, true);
        if ($isArray || $isOptionalValue) {
            $value = $value['value'];
        }

        if (is_array($value)) {
            $realValue = $this->widget(
                'DocParameterEntry',
                array(
                    'parameters' => $value,
                    'level' => $this->level + 1
                ),
                true
            );
        } else {
            $realValue = Yii::app()->controller->widget(
                'bootstrap.widgets.TbEditable',
                array(
                    'name' => $name,
                    'pk' => uniqid(),
                    'type' => 'text',
                    'text' => $this->extractValue($value),
                    'emptytext' => 'null',
                    'display' => 'js:function(value, sourceData) {
							if (value) {
								$(this).html("&quot;" + value + "&quot;");
							} else {
								$(this).html("null");
							}
						}',
                    'options' => array(
                        'clear' => $isOptionalValue
                    )
                ), true);
        }

        return $this->render('json-service.components.widgets.views.docParameter',
            array(
                'name' => $name,
                'value' => $realValue . $comma,
                'offset' => $this->getOffset(),
                'isOptionalValue' => $isOptionalValue,
                'isArray' => $isArray,
            ), true);
    }

    private function extractValue($value)
    {
        return is_array($value) ? $value['value'] : $value;
    }

    private function getOffset()
    {
        return str_repeat(' ', ($this->level) * self::OFFSET_SIZE);
    }

    private function getBracketOffset()
    {
        return str_repeat(' ', ($this->level - 1) * self::OFFSET_SIZE);
    }
}