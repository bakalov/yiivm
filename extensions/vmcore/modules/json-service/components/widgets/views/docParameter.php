<?php
echo CHtml::openTag('div', array('class' => 'node'));
echo $offset;
if ($isOptionalValue) {
    echo CHtml::tag('a', array(
        'class' => 'view remove-node-btn',
        'data-content' => 'Click for remove this optional node'
    ), '<i class="icon icon-remove"></i>');
    echo CHtml::openTag('span');
} else {
    echo CHtml::openTag('span', array('class' => 'red'));
}
echo sprintf('"%s"', $name);
echo CHtml::closeTag('span');
echo $isArray ? sprintf(': [%s]', $value) : sprintf(': %s', $value);
echo CHtml::closeTag('div');
?>