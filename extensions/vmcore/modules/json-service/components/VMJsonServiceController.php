<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of JsonServiceController
 *
 * @author Alex
 */

class VMJsonServiceController extends CController
{
	/**
	 * @var bool
	 */
	public $documentationMode = false;
	/**
	 * @var int
	 */
	protected $code = VMServiceResponseCode::NO_ERROR;
	/**
	 * @var null
	 */
	protected $request = null;
	/**
	 * @var array
	 */
	protected $data = array();
	/**
	 * @var array
	 */
	protected $exceptions = array();

	/**
	 * @param string $id
	 * @param null $module
	 */
	public function __construct($id, $module = null)
	{
		parent::__construct($id, $module);
		$this->code = VMServiceResponseCode::NO_ERROR;
		$this->exceptions = array();
	}

	/**
	 * @return null
	 */
	public function getRequest()
	{
		return $this->request;
	}

	public
	function init()
	{
		if ($this->documentationMode) {
			return;
		}

		if (Yii::app()->request->isPostRequest) {
			$json = json_decode(file_get_contents("php://input"), false);
			if (isset($json->request)) {
				$this->request = $json->request;
			} else {
				$this->respondWithError(VMServiceResponseCode::SERVICE_ERROR, 'There is no request node');
			}
		} else {
			$this->request = VMObjectUtils::fromArray($_GET);
		}
	}

	/**
	 * @param     $mixed
	 * @param int $code
	 */
	public
	function respondWithError($mixed, $code = VMServiceResponseCode::SERVICE_ERROR)
	{
		VMJsonServiceResponseBuilder::respondWithError($mixed, $code);
	}

	/**
	 * @param null $data
	 */
	public
	function respond($data = null)
	{
		VMJsonServiceResponseBuilder::build($data, null, VMServiceResponseCode::NO_ERROR);
	}

	/**
	 * @param array $params
	 *
	 * @throws VMDocumentationException
	 */
	public
	function checkInputParameters($params = array())
	{
		if ($this->documentationMode) {
			$exception = new VMDocumentationException();
			$exception->parameters = VMObjectUtils::fromArray($params);
			throw $exception;
		}
		$this->checkObjectParameter($params, $this->request);
	}

	/**
	 * @param $params
	 * @param $parent
	 */
	private
	function checkObjectParameter($params, $parent)
	{
		foreach ($params as $key => $value) {
			$isArray = is_array($value) && in_array('array', $value, true);
			$optional = is_array($value) && in_array('optional', $value, true);

			if ($isArray || $optional) {
				$value = $value['value'];
			}

			if (!$optional && $parent && !is_string($parent) && !array_key_exists($key, $parent)) {
				$this->respondWithError(sprintf('Missed input parameter -> %s', $key));
			}

			if ($isArray) {
				$array = array();

				if (is_array($parent) && array_key_exists($key, $parent)) {
					$array = $parent[$key] === NULL ? array() : $parent[$key];
				} else if(is_object($parent) && property_exists($parent, $key)) {
					$array = $parent->{$key} === null ? array() : $parent->{$key};
				}


				if (!is_array($array)) {
					$this->respondWithError(sprintf('Expected array in input parameter -> %s', $key));
				}

				foreach ($array as $item) {
					$this->checkObjectParameter($value, $item);
				}

			} else if (is_array($value) && isset($parent->{$key})) {
				$this->checkObjectParameter($value, (array)$parent->{$key});
			}
		}
	}

	public function run($actionId)
	{
		try {
			parent::run($actionId);
		} catch (CException $exception) {
			$this->respondWithError($exception->getMessage());
		}
	}

	public function filterAccessControl($filterChain)
	{
		$filter = new VMJsonServiceAccessControlFilter;
		$filter->setRules($this->accessRules());
		$filter->filter($filterChain);
	}

	/**
	 * @param $data
	 * @param $exceptions
	 * @param $code
	 */
	private
	function build($data, $exceptions, $code)
	{
		if ($this->documentationMode) {
			return;
		}

		VMJsonServiceResponseBuilder::build($data, $exceptions, $code);
	}
}

?>
