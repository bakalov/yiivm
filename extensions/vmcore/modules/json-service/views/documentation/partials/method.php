<div class="pull-right">
	<?=
	CHtml::link(Yii::t('vm', 'Call'), '#',
		array(
			'class' => 'btn btn-primary btn-large request-btn',
			'id' => $definition->name,
			'data-url' => Yii::app()->createAbsoluteUrl($definition->method)
		));
	?>
</div>


<p>
	<span><?= mb_strlen($definition->description) ? $definition->description : '&nbsp;'; ?></span>
</p>

<div>
	<h5><?=Yii::t('vm', 'Method')?></h5>
	<pre><?= $definition->method; ?></pre>
</div>

<div>
	<h5><?=Yii::t('vm', 'Request')?></h5>

	<?php

	$parameters = VMObjectUtils::toArray($definition->parameters);

	echo CHtml::tag('pre',
		array('id' => 'request-' . $definition->name),
		$this->widget('json-service.components.widgets.DocParameterEntry',
			array('parameters' => $parameters), true),
		true); ?>

</div>

<div class="hidden">
	<h5><?=Yii::t('vm', 'Response')?></h5>
	<?=
	CHtml::tag('div', array(
		'id' => 'response-' . $definition->name,
		'class' => 'pre'
	), 'Click on Call', true, true); ?>
</div>