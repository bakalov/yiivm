<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">
</head>

<body>
<div class="header">
    <?php
    $items = array();

    foreach ($controllers as $controllerData) {
        $controllerId = lcfirst(mb_ereg_replace('Controller', '', $controllerData->name));

        $items[] = array(
            'label' => VMTextUtils::humanizeCamel($controllerId),
            'url' => $this->createUrl('index', array('controller' => $controllerId)),
            'active' => $controllerId == $controller
        );
    }

    $this->widget('bootstrap.widgets.TbNavbar', array(
        'brand' => 'API Documentation',
        'fixed' => false,
        'items' => array(
            array(
                'class' => 'bootstrap.widgets.TbMenu',
                'items' => $items,
            )
        )
    ));
    ?>
</div>
<div class="container">
    <h2><?= VMTextUtils::humanizeCamel($controller) ?></h2>
    <?php

    Yii::setPathOfAlias('partials', __DIR__ . DIRECTORY_SEPARATOR . 'partials');

    $tabs = array();
    foreach ($definitions as $definition) {
        $tabs[] = array(
            'label' => VMTextUtils::humanizeCamel($definition->name),
            'content' => $this->renderPartial(
                'partials.method',
                array('definition' => $definition), true),
            'active' => count($tabs) == 0
        );
    }

    $this->widget(
        'bootstrap.widgets.TbTabs',
        array(
            'type' => 'tabs',
            'placement' => 'left',
            'tabs' => $tabs
        )
    );
    ?>
</div>

</body>
</html>