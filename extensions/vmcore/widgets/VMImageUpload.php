<?php
/**
 * @class VMImageUploadWidget
 * Description of VMImageUploadWidget class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMImageUpload extends CWidget {
	public $model;
	public $attribute;
	public $uploadImageUrl;
	public $removeImageUrl;

	public $width;
	public $height;

    public $submitWithForm = false;
    public $sendOnChange = true;

	public function init() {
		parent::init();

		$this->registerAssets();
	}

	public function run() {
		parent::run();

		$this->render('imageUpload', array(
			'model' => $this->model,
			'attribute' => $this->attribute,
			'width' => $this->width,
			'height' => $this->height,
			'uploadUrl' => $this->uploadImageUrl,
			'removeUrl' => $this->removeImageUrl,
            'submitWithForm' => $this->submitWithForm,
            'sendOnChange' => $this->sendOnChange
		));
	}

	protected function registerAssets() {
		$assetsPath = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'assets';
		$assetsUrl  = Yii::app()->assetManager->publish($assetsPath, false, -1, YII_DEBUG);
		/**
		 * @var CClientScript $cs
		 */
		$cs = Yii::app()->clientScript;
		$cs->registerScriptFile($assetsUrl . '/js/vm-image.js');
        $cs->registerScriptFile($assetsUrl . '/js/vm-file-input.js');
		$cs->registerScriptFile($assetsUrl . '/js/image-upload.js');
		$cs->registerCssFile($assetsUrl . '/css/imageUpload.css');
	}
} 