<?php
/**
 * @class VMImageUploadAction
 * Description of VMImageUploadAction class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMImageUploadAction extends CAction {
	public $model;
	public $attribute;

	public function run() {
		if(!$this->model) {
			throw new VMEntityException(Yii::t('vm', 'The "model" property cannot be empty.'));
		}

		if(!$this->attribute) {
			throw new VMEntityException(Yii::t('vm', 'The "attribute" property cannot be empty.'));
		}

		$attribute = $this->attribute;

		$saver = new VMEntitySaver($this->model);
		$saver->onAfterSave = function(CEvent $event) use ($attribute) {
			$upload = new VMUpload();
			$event->sender->entity->{$attribute} = $upload->quickSave($event->sender->entity, $attribute);
			$event->sender->entity->save();
		};

		if(!$saver->save()) {
			CJSON::encode($saver->entity->errors);
		}
	}
} 