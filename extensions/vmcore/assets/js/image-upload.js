/**
 * Created by nkolosov on 12/3/13.
 */

/* jsHint browser:true*/
/* global window:true */

(function ($, Error, FormData, undefined) {

    'use strict';

    function Form(jQ, options) {
        this.$ = jQ;
        this.options = {};
        this.options.url = jQ.attr('url') || jQ.attr('action');
        var data = jQ.data();
        this.options = jQ.extend(this.options, data, this.$default.options, options);

        var previewWrapper = jQ.find('.widget-crop-upload-image');
        this.$imgContainer = previewWrapper.find('.widget-crop-image');
        this.$removeBtn = previewWrapper.find('.widget-button-close');
        this.$editBtn = previewWrapper.find('.widget-button-file');
        this.$input = previewWrapper.find('.widget-input-file');
        this.attachListeners();
    }

    Form.prototype.$default = {
        options: {
            url: undefined,
            submitWithForm: false,
            sendOnChange: true
        }
    };

    Form.prototype.attachListeners = function () {
        this.$onAfterLoadPreview = Form.prototype.$onAfterLoadPreview.bind(this);

        this.$imgContainer.find('img').vmImage('crop', this.options);

        this.$removeBtn.on('click', Form.prototype.$onRemoveImage.bind(this));

        this.$input.on('change', Form.prototype.$onChangeImage.bind(this));

        this.$editBtn.on('click', Form.prototype.$onEditImage.bind(this));
    };

    Form.prototype.send = function (options, submitWithForm) {

        if (!this.options.sendOnChange) {
            return;
        }

        submitWithForm = submitWithForm || this.options.submitWithForm;

        var data = new FormData();
        if (submitWithForm) {
            $.map(this.$.find('input'), Form.prototype.$getDataItem.bind(this, data));
        } else {
            $.map(this.$.find('input[type="file"]'), Form.prototype.$getDataItem.bind(this, data));
        }

        var ajaxOptions = $.extend(
            {
                type: 'POST',
                url: this.options.url,
                processData: false,
                contentType: false
            },
            options,
            {
                data: data
            }
        );

        $.ajax(ajaxOptions);
    };

    Form.prototype.$getDataItem = function (container, input) {
        var $input = $(input);
        if ($input.attr('type').toLowerCase() === 'file') {
            this.$getFileData(container, input);
        }
        this.$getInputData(container, input);
    };

    Form.prototype.$getFileData = function (container, input) {
        $.map(input.files, function (file) {
            container.append($(input).attr('name'), file);
        });
    };

    Form.prototype.$getInputData = function (container, input) {
        var $input = $(input);
        container.append($input.attr('name'), $input.val());
    };

    Form.prototype.$onEditImage = function (event) {
        this.$input.click();
    };

    Form.prototype.$onRemoveImage = function (event) {
        this.send({
            url: this.options.removeUrl,
            context: this.$imgContainer,
            success: function () {
                $(this).empty();
                window.console.log('image removed');
            }
        });
    };

    Form.prototype.$onChangeImage = function (event) {
        $(event.target).vmFileInput('buildPreview', {
            afterLoadPreview: this.$onAfterLoadPreview
        });
    };

    Form.prototype.$onAfterLoadPreview = function (event) {
        var img = this.$imgContainer.find('img');
        if(img.length){
            img.remove();
        }
        this.$imgContainer.append(event.target);
        event.target.vmImage('crop', this.options);
        this.send({
            url: this.options.uploadUrl,
            success: function () {
                window.console.log('image uploaded');
            }
        });
    };

    $(window.document).ready(function () {
        $('.widget-crop-upload-image').each(function () {
            var $container = $(this);

            var options = $container.data();

            var $form = $container.closest('form');
            if (!$form.length) {
                $form = $container;
            }
            var form = new Form($form, options);

            $.data(this, 'imageUploadForm', form);
        });
    });

})(window.jQuery, window.Error, window.FormData);