<?php
/**
 * main.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:31 AM
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?= CHtml::encode($this->pageTitle); ?></title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="viewport" content="width=device-width,initial-scale=1">
</head>

<body>
<div class="header">

</div>

<div class="container">
	<h1>API Documentation</h1>

	<h2><?= $controller; ?></h2>

	<?php

	$tabs = array();
	foreach ($definitions as $definition) {
		$tabs[] = array(
			'label' => VMTextUtils::humanizeCamel($definition->name),
			'content' => $this->renderPartial(
					'vmcore.views.docs.partials.method',
					array('definition' => $definition), true),
			'active' => count($tabs) == 0
		);
	}

	$this->widget(
		'bootstrap.widgets.TbTabs',
		array(
			'type' => 'tabs',
			'placement' => 'left',
			'tabs' => $tabs
		)
	);?>

</div>


<style>
	.pre {
		display: block;
		padding: 9px;
		font-size: 13px;
		line-height: 20px;
		word-break: break-all;
		word-wrap: break-word;
		white-space: pre-wrap;
		background-color: #f5f5f5;
		border: 1px solid rgba(0, 0, 0, 0.15);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		margin: 1em 0em;
		color: #333333;
		font-family: Monaco, Menlo, Consolas, 'Courier New', monospace;
	}

	.non-pre {
		white-space: normal;
	}

</style>

<script type="text/javascript">

	(function ($) {
		'use strict';
		$('.request-btn').on('click', function () {
			var requestBlock = '#request-' + $(this).attr('id');
			var responseBlock = '#response-' + $(this).attr('id');
			var data = {request: JSON.parse($(requestBlock).text())};

			$.ajax({
				type: 'POST',
				context: $(responseBlock),
				url: $(this).data('url'),
				data: JSON.stringify(data),

				/**@this {jQuery}*/
				success: function (response, status, xhr) {
					this.parent().removeClass('hidden');

					var FORMAT_TAB_SIZE = 4;
					if (xhr.getResponseHeader('content-type') == 'application/json') {
						response = JSON.stringify(response, null, FORMAT_TAB_SIZE);
					} else {
						this.addClass('non-pre');
					}

					this.html(response);
				},
				error: function (response) {
					this.parent().removeClass('hidden');
					this.html(response.responseText);
				}
			});
		});
	})(window.jQuery);

</script>

</body>
</html>



