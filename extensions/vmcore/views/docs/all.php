<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?= CHtml::encode($this->pageTitle); ?></title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="viewport" content="width=device-width,initial-scale=1">
</head>

<body>

<div class="container">
	<h1>API Module Documentation</h1>
	<?php
		$tabs = array();

		foreach($controllers as $controller) {
			$controllerId = lcfirst(mb_ereg_replace('Controller', '', $controller));
			$tabs[] = array(
				'label' => $controllerId,
				'content' => $this->renderPartial('vmcore.views.docs.partials.controller', array(
						'controller' => $controllerId,
						'module' => $module,
					), true),
			);
		}

		$this->widget(
			'bootstrap.widgets.TbTabs',
			array(
				'type' => 'tabs',
				'placement' => 'left',
				'tabs' => $tabs,
			)
		);
	?>
</div>
</body>
</html>