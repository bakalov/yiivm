<?php

class VMObjectUtils
{
	/**
	 * @param      $object
	 * @param      $class
	 * @param bool $throwException
	 *
	 * @return bool
	 * @throws CException
	 */
	public static function checkClass($object, $class, $throwException = true)
	{
		$isClass = $object instanceof $class;

		if ($throwException && !$isClass) {
			throw new CException(
				sprintf('The object must be an object of %s or its descendant', $class));
		}

		return $isClass;
	}

	/**
	 * @param array $array
	 *
	 * @return stdClass
	 */
	public static function fromArray($array)
	{
		return CJSON::decode(CJSON::encode($array), false);
	}

	/**
	 * @param mixed $object
	 *
	 * @return array
	 */
	public static function toArray($object)
	{
		return CJSON::decode(CJSON::encode($object));
	}
}