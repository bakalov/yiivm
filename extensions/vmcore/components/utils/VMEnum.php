<?php

abstract class VMEnum
{
	/**
	 * @param $const
	 *
	 * @return stdClass
	 * @throws CException
	 */
	public static function get($const)
	{
		$meta = static::meta();
		if (!array_key_exists($const, $meta)) {
			throw new CException(Yii::t('vm', 'Invalid constant'));
		}

		return VMObjectUtils::fromArray($meta[$const]);
	}

	/**
	 * @param string $class
	 *
	 * @return array
	 */
	public static function getConstantsMap()
	{
		$reflection = new ReflectionClass(get_called_class());
		return $reflection->getConstants();
	}

	/**
	 * @return array
	 */
	protected static function meta() {}

	/**
	 * @param null|string $valueField
	 * @param string $labelField
	 *
	 * @return array
	 * @throws CException
	 */
	public static function listData($valueField = null, $labelField = null) {
		if(!$labelField) {
			throw new CException(Yii::t('vmcore.utils', 'Label field is not defined'));
		}

		$result = array();

		$reflection = new ReflectionClass(get_called_class());
		$constants = $reflection->getConstants();

		foreach($constants as $constValue) {
			$data = static::get($constValue);
			if($valueField && !isset($data->{$valueField})) {
				throw new CException(Yii::t('vmcore.utils', 'Value field not found'));
			}

			if(!isset($data->{$labelField})) {
				throw new CException(Yii::t('vmcore.utils', 'Label field not found'));
			}

			$key = ($valueField) ? $data->{$valueField} : $constValue;
			$result[$key] = $data->{$labelField};
		}

		return $result;
	}
}