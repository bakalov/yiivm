<?php

/**
 * Class VMTextUtils
 */
class VMTextUtils
{
	/**
	 * Gets a camel style string (like variable) and creates a user-friendly name for it.
	 * For example mainApplicationString -> Main Application String
	 * @param $variable . Input variable name
	 * @return user-friendly name of that variable
	 */
	public static function humanizeCamel($variable)
	{
		$result = ucfirst($variable);
		preg_match_all('/[A-Z]/', $result, $matches, PREG_OFFSET_CAPTURE);

		$index = 0;
		foreach ($matches[0] as $match) {
			$result = substr_replace($result, ' ', $match[1] + $index++, 0);
		}

		return trim($result);
	}

	/**
	 * Creates a string that could be used for url paths. It generates a user friendly name.
	 * For example Pirelli C. A&B -> pirelli-c-a-b
	 * @param $title Input parameter. Could be any string like Pirelli C. A&B
	 * @return Output user-friendly string pirelli-c-a-b
	 */
	public static function urlReadyString($title)
	{
		return strtolower(preg_replace('~[^\p{L}\p{N}]++~u', '-', $title));
	}

	/**
	 *
	 * @param string $label
	 * @example Convert "Camel Case" to "camel-case"
	 *
	 * @return string $label
	 */
	public static function labelToCode($label) {
		return mb_strtolower(mb_ereg_replace('\s', '', $label));
	}
}
