<?php

class VMConfigManager extends CApplicationComponent
{
	public $configs = array();

	public function init()
	{
		if (!$this->configs || !count($this->configs)) {
			throw new CException('No configurations detected. Please specify one or more');
		}

		foreach ($this->configs as $name => $configuration) {
			$instance = new VMConfig($name, $configuration);
			if ($instance->isActive()) {
				$instance->run();
			}
		}
	}
}