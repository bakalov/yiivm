<?php

class VMFileConfigDetector extends VMConfigDetector
{
	public function detected()
	{
		$filename = isset($this->params->filename) ? $this->params->filename : $this->name;

		if (!$filename) {
			throw new CException('filename parameter is null or missing');
		}
		$exists = file_exists(Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . $filename);
		return $exists;
	}
}