<?php

class VMUpload extends CComponent
{
	public $targetFolder = 'media';
	public $lastUploadedFile = null;

	public function init()
	{

	}

	private function prepareSaving(CModel $entity, $path) {
		if (!$path) {
			VMObjectUtils::checkClass($entity, 'CActiveRecord');

			/** @noinspection PhpParamsInspection */
			$path = $this->getUploadDirectoryOfEntity($entity);
		}

		$this->createDirectory($path);
	}

	private function generateFilename($entity, $attribute, $basename, $extension = null) {
		if ($basename) {
			$filename = $this->getRelativePathOfBasename($basename, $extension);
		} else {
			VMObjectUtils::checkClass($entity, 'CActiveRecord');

			/** @noinspection PhpParamsInspection */
			$filename = $this->getRelativePathOfEntity($entity, $attribute, $extension);
		}

		return $filename;
	}

	public function quickSave(CModel $entity, $attribute, $uploadDir = null, $basename = null)
	{
		$this->prepareSaving($entity, $uploadDir);

		if ($this->entityHasOwnProperty($entity, $attribute)) {
			$uploaded = CUploadedFile::getInstance($entity, $attribute);
		} else {
			$uploaded = CUploadedFile::getInstanceByName($attribute);
		}

		if ($uploaded) {
			$this->lastUploadedFile = $this->generateFilename($entity, $attribute, $basename, $uploaded->extensionName);

			if (!$uploaded->saveAs($this->getFullPathOf($this->lastUploadedFile))) {
				throw new CException(Yii::t('vmcore.media', 'Could not save file'));
			}

			return $this->lastUploadedFile;
		} else {
			return null;
		}
	}

	public function urlQuickSave(CModel $entity, $attribute, $url, $extension = null, $uploadDir = null, $basename = null)
	{
		$this->prepareSaving($entity, $uploadDir);

		$uploaded = file_get_contents($url);

		if ($uploaded) {
			$this->lastUploadedFile = $this->generateFilename($entity, $attribute, $basename, $extension);

			if (!file_put_contents($this->getFullPathOf($this->lastUploadedFile), $uploaded)) {
				throw new CException(Yii::t('vmcore.media', 'Could not save file'));
			}

			return $this->lastUploadedFile;
		} else {
			return null;
		}
	}

	public function base64quickSave(CModel $entity, $attribute, $extension, $uploadDir = null, $basename = null)
	{
		$this->prepareSaving($entity, $uploadDir);

		$uploaded = base64_decode($entity->{$attribute}, true);

		if ($uploaded) {
			$this->lastUploadedFile = $this->generateFilename($entity, $attribute, $basename, $extension);

			if (!file_put_contents($this->getFullPathOf($this->lastUploadedFile), $uploaded)) {
				throw new CException(Yii::t('vmcore.media', 'Could not save file'));
			}

			return $this->lastUploadedFile;
		} else {
			return null;
		}
	}

	public function getUploadDirectoryOfEntity(CActiveRecord $entity)
	{
		return $this->getUploadDirectory() . DIRECTORY_SEPARATOR . $entity->tableName();
	}

	public function getUploadDirectory()
	{
		return sprintf('%s/../%s', Yii::app()->basePath, $this->targetFolder);
	}

	protected function createDirectory($directory)
	{
		if (!file_exists($directory)) {
			mkdir($directory, 0777, true);
		}
	}

	protected function entityHasOwnProperty(CModel $entity, $prop)
	{
		return $entity->hasProperty($prop) || isset($entity[$prop]) || property_exists($entity, $prop);
	}

	public function getRelativePathOfBasename($basename, $extension)
	{
		if ($extension) {
			$basename .= '.' . $extension;
		}

		return $basename;
	}

	public function getRelativePathOfEntity(CActiveRecord $entity, $attribute, $extension = null)
	{
		if (is_array($entity->primaryKey)) {
			$key = implode('-', $entity->primaryKey);
		} else {
			$key = $entity->primaryKey;
		}

		$basename = $this->targetFolder . DIRECTORY_SEPARATOR . $entity->tableName() . DIRECTORY_SEPARATOR . $key . '-' . $attribute;
		return $this->getRelativePathOfBasename($basename, $extension);
	}

	public function getFullPathOf($filename)
	{
		return Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . $filename;
	}

	public function quickRemove($mixed, $attribute)
	{
		if(!$this->havePermission($mixed, $attribute)) {
			return false;
		}

		$result = true;

		if (is_array($mixed)) {
			foreach ($mixed as $model) {
				$result = $result && $this->quickRemoveFile($model, $attribute);
			}
		} else {
			$result = $this->quickRemoveFile($mixed, $attribute);
		}

		return $result;
	}

	private function havePermission($mixed, $attribute)
	{
		if (is_array($mixed)) {
			foreach ($mixed as $model) {
				$path = $this->getFullPathOf($model->{$attribute});
				if (!is_writable($path)) {
					return false;
				}
			}
		} else {
			$path = $this->getFullPathOf($mixed->{$attribute});
			if (!is_writable($path)) {
				return false;
			}
		}

		return true;
	}

	private function quickRemoveFile($model, $attribute)
	{
		try {
			if (!$model->{$attribute}) {
				return false;
			}

			$path = $this->getFullPathOf($model->{$attribute});
			if (is_writable($path)) {
				unlink($path);
			}
		} catch (CException $exception) {
			return false;
		}

		return true;
	}
}