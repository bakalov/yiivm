<?php
/**
 * @class VMCreditCard
 * Description of VMCreditCard class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMCreditCard extends CModel {
	public $type;
	public $firstName;
	public $lastName;
	public $number;
	public $expireMonth;
	public $expireYear;
	public $cvc;

	public function rules()
	{
		return array(
			array('type, firstName, lastName, number, expireMonth, expireYear, cvc', 'safe'),
			array('type', 'in', 'range' => VMCreditCardType::getTypes()),
		);
	}

	public function attributeLabels()
	{
		return array(
			'firstName' => 'Cardholder First Name',
			'lastName'  => 'Cardholder Last Name',
			'number'    => 'Card Number',
			'expireMonth' => 'Card Expire Month',
			'expireYear'  => 'Card Expire Year',
			'cvc'         => 'Card CVC code',
		);
	}

	public function attributeNames()
	{
		$class = new ReflectionClass(get_class($this));
		$names = array();

		foreach($class->getProperties() as $property)
		{
			$name = $property->getName();
			if($property->isPublic() && !$property->isStatic())
				$names[] = $name;
		}

		return $names;
	}
}