<?php
use PayPal\Api\Amount;
use PayPal\Api\CreditCard;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PPConnectionException;
use PayPal\Rest\ApiContext;

/**
 * @class VMPayPalPaymentProvider
 * Description of VMPayPalPaymentProvider class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */

spl_autoload_unregister(array('YiiBase', 'autoload'));
require_once __DIR__ . '/vendors/PayPal/autoload.php';
spl_autoload_register(array('YiiBase', 'autoload'));

class VMPayPalPaymentProvider extends VMBasePaymentProvider {
	const PAYMENT_CREDITCARD = 'credit_card';
	const PAYMENT_PAYPAL = 'paypal';

	const MODE_LIVE = 'live';
	const MODE_SANDBOX = 'sandbox';

	/**
	 * Paypal application clientId
	 *
	 * @var string $clientId
	 */
	public $clientId;

	/**
	 * Paypal application secret
	 *
	 * @var string $secret
	 */
	public $secret;

	/**
	 * Sandbox or Live mode
	 *
	 * @var string $mode
	 */
	public $mode = self::MODE_LIVE;

	/**
	 * Information about customer credit card
	 *
	 * @var CreditCard $creditCard
	 */
	public $creditCard;

	/**
	 * Amount of payment
	 *
	 * @var Amount $amount
	 */
	public $amount;

	/**
	 * Payment method (credit card or PayPal)
	 *
	 * @var string $paymentMethod
	 */
	public $paymentMethod;

	/**
	 * Return url for PayPal service
	 *
	 * @var string $returnUrl
	 */
	public $returnUrl;

	/**
	 * Cancel url for PayPal service
	 *
	 * @var string $cancelUrl
	 */
	public $cancelUrl;

	public $approvalUrl;
	public $executeUrl;

	/**
	 * @param array $options
	 */
	public function __construct($options = array()) {
		$this->setOptions($options);
	}

	/**
	 * Set redirect url for PayPal service
	 *
	 * @param string $url
	 */
	public function setReturnUrl($url) {
		$this->returnUrl = $url;
	}

	/**
	 * Set cancel url for PayPal service
	 *
	 * @param string $url
	 */
	public function setCancelUrl($url) {
		$this->cancelUrl = $url;
	}

	public function getApiContext()
	{
		if(!$this->clientId) {
			throw new CException(Yii::t('vm', 'The "clientId" property cannot be empty.'));
		}

		if(!$this->secret) {
			throw new CException(Yii::t('vm', 'The "secret" property cannot be empty.'));
		}

		$credential = new OAuthTokenCredential($this->clientId, $this->secret);

		$apiContext = new ApiContext($credential, 'Request' . time());
		$apiContext->setConfig(array(
			'mode' => $this->mode,
		));

		return $apiContext;
	}

	public function execute($paymentId, $payerId)
	{
		try {
			$payment = Payment::get($paymentId, $this->getApiContext());

			$execution = new PaymentExecution();
			$execution->setPayerId($payerId);

			$payment->execute($execution, $this->getApiContext());

			return true;
		} catch (PPConnectionException $ex) {
			$response = CJSON::decode($ex->getData(), false);
			$this->errors = $response->message;
		}

		return false;
	}

	public function sendPayment($description = '')
	{
		try {
			$payment = new Payment();
			$payment->setIntent("sale");

			$payer = new Payer();
			$payer->setPaymentMethod($this->paymentMethod);

			if($this->paymentMethod == self::PAYMENT_CREDITCARD) {
				$fundingInstrument = new FundingInstrument();
				$fundingInstrument->setCreditCard($this->creditCard);

				$payer->setFundingInstruments(array($fundingInstrument));

			} else if ($this->paymentMethod == self::PAYMENT_PAYPAL) {
				if(!$this->returnUrl) {
					throw new CException(Yii::t('vm', 'The "returnUrl" property cannot be empty.'));
				}

				if(!$this->cancelUrl) {
					throw new CException(Yii::t('vm', 'The "cancelUrl" property cannot be empty.'));
				}

				$redirectUrls = new RedirectUrls();
				$redirectUrls->setReturnUrl($this->returnUrl);
				$redirectUrls->setCancelUrl($this->cancelUrl);

				$payment->setRedirectUrls($redirectUrls);
			}

			$transaction = new Transaction();
			$transaction->setAmount($this->amount);
			$transaction->setDescription($description);

			$payment->setPayer($payer);
			$payment->setTransactions(array($transaction));

			$payment->create($this->getApiContext());

			$this->paymentId = $payment->getId();

			if($this->paymentMethod == self::PAYMENT_PAYPAL) {
				$links = $payment->getLinks();
				$this->approvalUrl = $links[1]->getHref();
				$this->executeUrl = $links[2]->getHref();
			}

			return true;

		} catch(PPConnectionException $ex) {
			$response = CJSON::decode($ex->getData(), false);
			$this->errors = $response->message;

			//var_dump($ex->getData());
		} catch (Exception $ex) {

		}

		return false;
	}

	/**
	 * @param VMCreditCard $creditCard
	 */
	public function setCardDetails($creditCard)
	{
		$card = new CreditCard();
		$card->setType(mb_strtolower($creditCard->type));
		$card->setNumber($creditCard->number);
		$card->setExpireMonth($creditCard->expireMonth);
		$card->setExpireYear($creditCard->expireYear);
		$card->setFirstName($creditCard->firstName);
		$card->setLastName($creditCard->lastName);
		$card->setCvv2($creditCard->cvc);

		$this->creditCard = $card;
	}

	/**
	 * Set payment currency and total amount
	 *
	 * @param VMAmount $amount
	 */
	public function setAmount($amount)
	{
		$amountDetails = new Amount();
		$amountDetails->setCurrency($amount->currency);
		$amountDetails->setTotal(sprintf("%.2f", $amount->amount));

		$this->amount = $amountDetails;
	}

	/**
	 * Set payment method (credit card or PayPal)
	 *
	 * @param string $paymentMethod
	 */
	public function setPaymentMethod($paymentMethod = self::PAYMENT_CREDITCARD)
	{
		$this->paymentMethod = $paymentMethod;
	}

	/**
	 * Enable or disable sandbox mode
	 *
	 * @param bool $sandbox
	 */
	public function setMode($mode = self::MODE_SANDBOX) {
		$this->mode = $mode;
	}
} 