<?php
/**
 * @class VMPaymentManager
 * Description of VMPaymentManager class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMPaymentManager {
	/**
	 * @var integer payment using PayPal
	 */
	const PROVIDER_PAYPAL = 1;
	/**
	 * @var integer payment using Realex
	 */
	const PROVIDER_REALEX = 2;

	/**
	 * @var VMPaymentProvider $provider
	 */
	public $provider;

	/**
	 * @param mixed $provider integer or VMPaymentProvider
	 * @param array $options
	 *
	 * @throws CException
	 */
	public function __construct($provider, $options = array()) {
		if($provider instanceof VMPaymentProvider) {
			$this->provider = $provider;
		} else {
			switch ($provider) {
				case self::PROVIDER_PAYPAL:
					$this->provider = new VMPayPalPaymentProvider($options);
					break;
				case self::PROVIDER_REALEX:
					$this->provider = new VMRealexPaymentProvider($options);
					break;
				default:
					throw new CException(Yii::t('vm', 'Invalid payment provider'));
			}
		}
	}

	public function setOptions($options = array()) {
		$this->provider->setOptions($options);
	}

	public function setAmount($amount) {
		$this->provider->setAmount($amount);
	}

	/**
	 * @param VMCreditCard $creditCard
	 */
	public function setCardDetails($creditCard) {
		$this->provider->setCardDetails($creditCard);
	}

	public function sendPayment($description = '') {
		return $this->provider->sendPayment($description);
	}

	public function getErrors() {
		return $this->provider->errors;
	}

	public function getOption($optionName = '') {
		if(property_exists(get_class($this->provider), $optionName)) {
			return $this->provider->{$optionName};
		}

		return null;
	}

} 