<?php

return array(
	'Email' => 'E-Mail',
	'Password' => 'Пароль',
	'Remember Me' => 'Запомнить меня',
	'Incorrect email or password' => 'Неверный e-mail или пароль',
);