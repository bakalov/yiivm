<?php
/**
 * @class VMBasePushProvider
 * Description of VMBasePushProvider class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMBasePushProvider implements VMPushProvider {
	public $applicationKey = null;
	public $apiKey = null;

	public $response = null;
	public $code     = null;
	public $errors   = null;
	public $message = null;

	/**
	 * @param array $config
	 */
	public function __construct($config = array(), $className = __CLASS__)
	{
		if($config) {
			foreach($config as $configKey => $configValue) {
				if(property_exists($className, $configKey)) {
					$this->{$configKey} = $configValue;
				} else {
					$this->setResponse(self::STATUS_BAD_PARAM, sprintf("%s have not a field %s", $className, $configKey));
				}
			}
		}
	}

	/**
	 * Set application key for provider
	 *
	 * @param string $applicationKey
	 * @return void
	 */
	public function setApplicationKey($applicationKey)
	{
		$this->applicationKey = $applicationKey;
	}

	/**
	 * Set API key or API-token for provider
	 *
	 * @param string $apiKey
	 * @return void
	 */
	public function setApiKey($apiKey)
	{
		$this->apiKey = $apiKey;
	}

	/**
	 * Send push notification to devices
	 *
	 * @param string $message Message for sending
	 *
	 * @return boolean true if successful or false otherwise
	 */
	public function sendPush($message)
	{
		// TODO: Implement sendPush() method.
	}

	/**
	 * Return a server response if exists
	 *
	 * @return mixed
	 */
	public function getResponse()
	{
		$this->response = new StdClass;
		$this->response->code = $this->getCode();
		$this->response->message = $this->message;

		return $this->response;
	}

	/**
	 * Return a errors if exists
	 *
	 * @return mixed
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * Return a status code
	 *
	 * @return integer
	 */
	public function getCode()
	{
		return $this->code;
	}

	/**
	 * Set response code and response message
	 *w
	 * @param integer $code
	 * @param string $message
	 * @param mixed $data
	 *
	 * @return void
	 */
	public function setResponse($code, $message, $data = array()) {
		$this->code   = $code;
		$this->message = $message;
		$this->errors = $message;
	}

    /**
     * Send push notification to devices
     *
     * @param VMPushModel $data Message for sending
     *
     * @return boolean true if successful or false otherwise
     */
    public function sendRichPush(VMPushModel $data)
    {
        // TODO: Implement sendRichPush() method.
    }

}