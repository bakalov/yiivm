<?php
/**
 * @class VMXtremePushProvider
 * VMXtremePushProvider class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMXtremePushProvider extends VMBasePushProvider{

	public $tokenArray = array();

	/**
	 * @param array $config
	 */
	public function __construct($config = array(), $className = __CLASS__)
	{
		parent::__construct($config, $className);
	}

	/**
	 * Send push notification for devices
	 *
	 * @param string $message Message for sending
	 *
	 * @return boolean true if successful or false otherwise
	 */
	public function sendPush($message)
	{
		if($message === null) {
			$this->setResponse(self::STATUS_BAD_PARAM, 'You must specify a message for sending');
			return false;
		}

		$data = array(
			'apptoken' => $this->apiKey,
			'PushMessage' => array(
				'text' => $message,
				'ios' => array(
					'active' => 1,
					'environment' => 'sandbox',
				),
			)
		);

		if(!empty($this->tokenArray)) {
			$data['PushMessage']['tokenArray'] = $this->tokenArray;
		}

		if($curl = curl_init()) {
			curl_setopt($curl, CURLOPT_URL, 'https://xtremepush.com/api/push/pushMessage/create');
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

			$this->response = CJSON::decode(curl_exec($curl));

			curl_close($curl);

			if(isset($this->response['success']) && $this->response['success'] == true) {
				$data = array(
					'apptoken' => $this->apiKey,
					'id' => $this->response['model']['data']['id'],
				);

				$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, 'https://xtremepush.com/api/push/pushMessage/send');
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

				$response = curl_exec($curl);
				curl_close($curl);

				$this->setResponse(self::STATUS_OK, "Notification has been successfully sent");
				return true;
			} else {
				$this->setResponse($this->response['code'], $this->response['message']);
			};
		}

		return false;
	}

	/**
	 * Set response code and response message
	 *
	 * @param integer $code
	 * @param string $message
	 * @param mixed $data
	 *
	 * @return void
	 */
	public function setResponse($code, $message, $data = array())
	{
		$this->code = $code;
		$this->message = $message;
		$this->errors = $message;
	}

}