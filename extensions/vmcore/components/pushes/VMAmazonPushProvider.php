<?php
use Aws\CloudFront\Exception\Exception;
use Aws\Common\Aws;
use Aws\S3\S3Client;
use Aws\Sns\SnsClient;

spl_autoload_unregister(array('YiiBase', 'autoload'));
require_once __DIR__ . '/vendors/Aws/Autoloader.php';
spl_autoload_register(array('YiiBase', 'autoload'));

/**
 * @class VMAmazonPushProvider
 * Description of VMAmazonPushProvider class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMAmazonPushProvider extends VMBasePushProvider
{
    public $region;
    public $application;
    public $client;

    private $target;

    public function __construct($config = array(), $className = __CLASS__)
    {
        parent::__construct($config, $className);

        $this->client = SnsClient::factory(array(
            'key' => $this->applicationKey,
            'secret' => $this->apiKey,
            'region' => $this->region,
        ));
    }

    private function  send($alert, $data, $badge)
    {
        if (!$this->target) {
            $this->setResponse(self::STATUS_BAD_PARAM, 'You must specify a target for sending');
            return false;
        }

        if (!$alert) {
            $this->setResponse(self::STATUS_BAD_PARAM, 'You must specify a message for sending');
            return false;
        }

        try {
            $this->client->publish(array(
                'MessageStructure' => 'json',
                'Message' => CJSON::encode(array(
                    'default' => $alert,
                    'APNS' => CJSON::encode(array(
                        'aps' => array(
                            'alert' => $alert,
                            'badge' => $badge
                        ),
                        // Custom payload parameters can go here
                        'data' => $data
                    ))
                )),
                'TargetArn' => $this->target
            ));

            $this->setResponse(self::STATUS_OK, "Notification has been successfully sent");
            return true;
        } catch (Exception $e) {
            $this->setResponse($e->getCode(), $e->getMessage());
            return false;
        }
    }

    public function sendPush($alert, $data = NULL)
    {
        return $this->send($alert, $data, NULL);
    }

    public function sendPushWithBadge($alert,$badge)
    {
        return $this->send($alert, NULL, $badge);
    }

    public function sendRichPush(VMPushModel $model)
    {
        if (!$this->target) {
            $this->setResponse(self::STATUS_BAD_PARAM, 'You must specify a target for sending');
            return false;
        }

        if (!$model->alert) {
            $this->setResponse(self::STATUS_BAD_PARAM, 'You must specify a message for sending');
            return false;
        }

        try {
            $this->client->publish(array(
                'MessageStructure' => 'json',
                'Message' => CJSON::encode(array(
                    'default' => $model->alert,
                    'APNS' => CJSON::encode(array(
                        'aps' => array(
                            'alert' => $model->alert,
                            'badge' => $model->badge,
                            'sound' => $model->sound
                        ),
                        // Custom payload parameters can go here
                        'data' => $model->data
                    ))
                )),
                'TargetArn' => $this->target
            ));

            $this->setResponse(self::STATUS_OK, "Notification has been successfully sent");
            return true;
        } catch (Exception $e) {
            $this->setResponse($e->getCode(), $e->getMessage());
            return false;
        }
    }

    public function registerTarget($deviceToken)
    {
        $response = $this->client->createPlatformEndpoint(array(
            'PlatformApplicationArn' => $this->application,
            'Token' => $deviceToken,
        ));

        return $response['EndpointArn'];
    }

    public function setTarget($target)
    {
        $this->target = $target;
    }
}