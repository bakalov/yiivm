<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Alex
 * Date: 19.10.13
 * Time: 3:35 AM
 * To change this template use File | Settings | File Templates.
 */

class VMWebUser extends CWebUser
{
    public $userClass;
    /**@var CActiveRecord $userModel */
    private $userModel = null;

	private function initializeUserModel() {
		if($this->userClass === null) {
			throw new CException('The user class is not set up properly. Please set up userClass property in your config file');
		}

		$userId = parent::getId();

		if ($userId && !$this->userModel) {
			$this->userModel = CActiveRecord::model($this->userClass)->findByPk($userId);
		}
	}

    public function __get($name)
    {
	    $this->initializeUserModel();

	    $userId = parent::getId();

        if ($userId) {
            if ($this->userModel && $this->modelHasOwnProperty($name)) {
                return $this->userModel->{$name};
            }
        }

        return parent::__get($name);
    }

	public function __call($name, $parameters)
	{
		$this->initializeUserModel();

		$userId = parent::getId();

		if ($userId) {
			if (method_exists($this->userModel, $name)) {
				return call_user_func_array(array($this->userModel, $name), $parameters);
			}
		}

		return parent::__call($name, $parameters);
	}


	public function getRole() {
		return null;
	}

    private function modelHasOwnProperty($name)
    {
        return $this->userModel->hasAttribute($name) ||
        $this->userModel->hasProperty($name) ||
        array_key_exists($name, $this->userModel->relations());
    }
}