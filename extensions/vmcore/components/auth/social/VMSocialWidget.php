<?php
/**
 * @class VMSocialWidget
 * Description of VMSocialWidget class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */

class VMSocialWidget extends EAuthWidget
{
	public $view = 'index';

	public function run()
	{
		$this->registerAssets();
		$this->render($this->view, array(
			'id' => $this->getId(),
			'services' => $this->services,
			'action' => $this->action,
		));
	}
} 