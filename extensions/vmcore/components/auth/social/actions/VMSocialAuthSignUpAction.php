<?php
/**
 * @class VMSocialAuthSignUpAction
 * Description of VMSocialAuthSignUpAction class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 * @deprecated {@see VMSocialConnectAction}
 */

class VMSocialAuthSignUpAction extends CAction {
	public $userClass;
	public $returnUrl;
	public $cancelUrl;
	public $componentName;
	public $identityField = 'id';

	public function run() {
		if (!$this->userClass) {
			throw new CException('userClass is not set up properly');
		}

		if(!$this->componentName) {
			throw new CException('componentName is not set up properly');
		}

		if(!$this->returnUrl) {
			$this->returnUrl = Yii::app()->user->returnUrl;
		}

		$service = Yii::app()->request->getParam('service');

		if (isset($service)) {
			/**
			 * @var VMSocialAuth $component
			 */
			$component = Yii::app()->getComponent($this->componentName);
			$authIdentity = $component->getIdentity($service);
			$authIdentity->redirectUrl = $this->returnUrl;
			$authIdentity->cancelUrl = $this->cancelUrl;

			if ($authIdentity->authenticate()) {
				Yii::app()->user->setState('service', $service);
				Yii::app()->user->setState('identity', $authIdentity->id);

				$identity = new VMSocialIdentity($this->userClass, $authIdentity, $this->identityField);

				if ($identity->authenticate()) {
					Yii::app()->user->login($identity);
					$authIdentity->redirect();
				} else {
					Yii::app()->user->setState('socialAttributes', (object) $authIdentity->attributes);

					$authIdentity->redirect();
				}
			} else {
				$authIdentity->cancel();
			}
		}
	}
}