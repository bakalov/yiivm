<?php

/**
 * @class  VMSocialConnectAction
 * Description of VMSocialConnectAction class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMSocialConnectAction extends CAction
{
	public $userClass;
	public $returnUrl;
	public $cancelUrl;
	public $componentName;
	public $identityField = 'id';

	public function run()
	{
		if (!$this->userClass) {
			throw new CException('userClass is not set up properly');
		}

		if (!$this->componentName) {
			throw new CException('componentName is not set up properly');
		}

		if (!$this->returnUrl) {
			$this->returnUrl = Yii::app()->user->returnUrl;
		}

		$service = Yii::app()->request->getQuery('service');

		if(!$service) {
			throw new CHttpException(400, 'Bad request. Service not setting up');
		}

		/**
		 * @var VMSocialAuth $component
		 */
		$component = Yii::app()->getComponent($this->componentName);
		$authIdentity              = $component->getIdentity($service);
		$authIdentity->redirectUrl = $this->returnUrl;
		$authIdentity->cancelUrl   = ($this->cancelUrl) ? $this->cancelUrl : $this->returnUrl;

		try {
			if ($authIdentity->authenticate()) {
				Yii::app()->user->setState('service', $service);
				Yii::app()->user->setState('identity', $authIdentity->id);
				Yii::app()->user->setState('socialAttributes', (object) $authIdentity->attributes);

				$identity = new VMSocialIdentity($this->userClass, $authIdentity, $this->identityField);

				if ($identity->authenticate()) {
					Yii::app()->user->login($identity);
				}

				$authIdentity->redirect();
			}

			$authIdentity->cancel();
		} catch (EAuthException $e) {
			Yii::app()->user->setFlash('error', 'EAuthException: ' . $e->getMessage());
			$authIdentity->cancel();
		}
	}
}