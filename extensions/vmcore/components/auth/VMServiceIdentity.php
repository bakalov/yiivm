<?php

class VMServiceIdentity extends CUserIdentity
{
	public $tokenClass;
	public $token;
	public $onAuthorization = null;
	public $idAttribute = 'user_id';
	private $id;
	private $isAuthenticated;

	/**
	 * @param string $hash
	 * @param string $tokenClass . tokens class name
	 */
	public function __construct($hash, $tokenClass, $onAuthorization = false)
	{
		parent::__construct($hash, null);
		$this->tokenClass = $tokenClass;
		$this->onAuthorization = $onAuthorization;
	}

	public function authenticate()
	{
		/**@var CActiveRecord $token */
		$token = new $this->tokenClass;
		$this->token = $token->findByAttributes(array('hash' => $this->username));
		if (!$this->token) {
			return $this->isAuthenticated = false;
		}
		$this->id = $this->token->{$this->idAttribute};

		if (is_callable($this->onAuthorization)) {
			call_user_func($this->onAuthorization, new CEvent($this));
		}

		return $this->isAuthenticated = true;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getIsAuthenticated()
	{
		return $this->isAuthenticated;
	}
}