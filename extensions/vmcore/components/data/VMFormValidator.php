<?php

class VMFormValidator extends CComponent
{
	private $formClass;
	private $form;

	public function __construct($formClass)
	{
		$this->formClass = $formClass;
	}

	public function validate($scenario = null, $ajaxValidate = false)
	{
		if (!$this->formClass) {
			throw new CException('The formClass property is not initialized');
		}

		$result = false;

		$this->form = new $this->formClass($scenario);
		$attributes = Yii::app()->request->getParam($this->formClass);

		if ($attributes) {
			$this->form->attributes = $attributes;

			if($ajaxValidate) {
				$errors = CActiveForm::validate($this->form);

				echo $errors;

				if($errors == CJavaScript::encode(array())) {
					$result = true;
				}
			} else {
				$result = $this->form->validate();
			}
		}

		return $result;
	}

	public function getForm()
	{
		return $this->form;
	}
}