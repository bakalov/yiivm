<?php

class VMEntityConverter
{
	public static function json($mixed, $relations = null, $eachCallback = null)
	{
		if (!$mixed) {
			return null;
		}

		if (is_array($mixed)) {
			return self::dataFromModelArray($mixed, $relations, $eachCallback);
		} else {
			return self::dataFromModel($mixed, $relations, $eachCallback);
		}
	}

	private static function dataFromModelArray(array $modelArray, $relations = null, $eachCallback = null)
	{
		$result = array();
		foreach ($modelArray as $model) {
			$data = self::dataFromModel($model, $relations, $eachCallback);
			array_push($result, $data);
		}
		return $result;
	}

	private static function dataFromModel(CModel $model, $relations = null, $eachCallback = null)
	{
		if (!$model)
			return null;

		$attributes = $model->attributes;

		if ($eachCallback) {
			$result = call_user_func($eachCallback, $model);
			$attributes = array_merge($attributes, $result);
		}

		if ($model instanceof CActiveRecord && $relations) {
			foreach ($relations as $relation) {
				$relationName = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $relation));
				$relationship = $model->getRelated($relation);
				if ($relationship) {
					if (is_array($relationship)) {
						foreach ($relationship as $object) {
							$attributes[$relationName][] = $object->attributes;
						}
					} else {
						$attributes[$relationName] = $relationship->attributes;
					}
				}
			}
		}

		return $attributes;
	}
}
