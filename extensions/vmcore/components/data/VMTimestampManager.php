<?php
/**
 * Created by JetBrains PhpStorm.
 * User: egor
 * Date: 13.01.14
 * Time: 16:45
 * To change this template use File | Settings | File Templates.
 */
class VMTimestampManager extends CApplicationComponent
{
    protected $settings;

    public function init()
    {
        $this->settings = VMObjectUtils::fromArray(array(
            'rowNames' => array(
                'createdAt' => 'created_at',
                'updatedAt' => 'updated_at'
            ),
            'timestampFunction' => 'UTC_TIMESTAMP'
        ));
    }

    public function getRows()
    {
        return $this->settings->rowNames;
    }

    public function setRows(array $rowNames)
    {
        return $this->settings->rowNames = $rowNames;
    }

    public function getCreatedAtRow()
    {
        return $this->settings->rowNames->createdAt;
    }

    public function setCreatedAtRow($rowName)
    {
        return $this->settings->rowNames->createdAt = $rowName;
    }

    public function getUpdatedAtRow()
    {
        return $this->settings->rowNames->updatedAt;
    }

    public function setUpdatedAtRow($rowName)
    {
        return $this->settings->rowNames->updatedAt = $rowName;
    }

    public function getTimestampFunction()
    {
        return $this->settings->timestampFunction;
    }

    public function setTimestampFunction($timestampFunction)
    {
        return $this->settings->timestampFunction = strtoupper($timestampFunction);
    }

    public function setTimestamp(CActiveRecord &$model)
    {
        $rowNames = $this->settings->rowNames;
        $timestampFunction = $this->settings->timestampFunction;
        if ($model->isNewRecord) {
            if ($model->hasAttribute($rowNames->createdAt)) {
                $model->{$rowNames->createdAt} = new CDbExpression($timestampFunction);
            }
        }
        if ($model->hasAttribute($rowNames->updatedAt)) {
            $model->{$rowNames->updatedAt} = new CDbExpression($timestampFunction);
        }
    }
}