<?php

class VMFormModelException extends VMModelException
{
	public function __construct(CFormModel $model)
	{
		parent::__construct($model);
	}
}