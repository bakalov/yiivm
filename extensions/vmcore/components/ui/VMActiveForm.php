<?php

class VMActiveForm extends TbActiveForm
{
	public $options = array();
	public $htmlOptions = array();

	public function datetimePickerRow($model, $fieldName, $options = array(), $htmlOptions = array())
	{
		Yii::import('yiivm.extensions.datetime-picker.*');
		echo CHtml::tag('div', array('class' => 'control-group'), false, false);
		echo CHtml::tag('label', array('class' => 'control-label', 'for' => get_class($model) . '_' . $fieldName), false, false);

		echo $model->getAttributeLabel($fieldName);
		echo CHtml::closeTag('label');

		echo CHtml::tag('div', array('class' => 'controls'), false, false);

		$mode = (isset($options['mode']) && (in_array($options['mode'], array('time', 'date', 'datetime')))) ?
			$options['mode'] : 'datetime';

		$this->widget('CJuiDateTimePicker', array(
			'model' => $model,
			'attribute' => $fieldName,
			'mode' => $mode, //use "time","date" or "datetime" (default)
			'language' => '',
			'htmlOptions' => $htmlOptions,
			'options' => $options // jquery plugin options
		));

		echo CHtml::closeTag('div');
		echo CHtml::closeTag('div');
	}

	public function imageUploadRow($model, $attribute, $options = array())
	{
		$this->widget('VMImageUploadWidget', array(
			'model' => $model,
			'attribute' => $attribute,
			'width' => $options['width'],
			'height' => $options['height'],
			'uploadImageUrl' => $options['uploadImageUrl'],
			'removeImageUrl' => $options['removeImageUrl'],
		));
	}
}